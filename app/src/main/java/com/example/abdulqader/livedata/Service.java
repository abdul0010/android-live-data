package com.example.abdulqader.livedata;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Service {
    @GET("top_rated?")
    Call<MovieResult> getTopRatedMovies();
    @GET("popular?")
    Call<MovieResult> getPopularMovies();
}
