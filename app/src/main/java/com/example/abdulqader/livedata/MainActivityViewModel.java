package com.example.abdulqader.livedata;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.logging.Handler;

public class MainActivityViewModel extends ViewModel {
    private MutableLiveData<List<String>> listMutableLivedata;
    public LiveData<List<String>> getUsername(){
        if(listMutableLivedata==null){
            listMutableLivedata=new MutableLiveData<>();
          //  loadUsername();

        }

        return listMutableLivedata;
    }

//    private void loadUsername() {
//        Handler myHandler = new Handler();
//        myHandler.postDelayed(() -> {
//            List<String> fruitsStringList = new ArrayList<>();
//            fruitsStringList.add("@code_wizard");
//            fruitsStringList.add("@ninja_developer");
//            fruitsStringList.add("@denzel");
//            fruitsStringList.add("@bananaPeel");
//            fruitsStringList.add("@kioko");
//            long seed = System.nanoTime();
//            Collections.shuffle(fruitsStringList, new Random(seed));
//
//            listMutableLivedata.setValue(fruitsStringList);
//        }, 5000);
//
//    }
}

